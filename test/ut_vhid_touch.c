#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <linux/uinput.h>

//open()
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>

//read()
#include <unistd.h>

// should be last
#include <cmocka.h>
#include "vhid_device.h"
#include "vhid_remote_dev.h"

static void vhid_touch_create_dev_null_msg(void **state)
{
	struct vhid_dev vdev;
	will_return(__wrap_malloc, 0);
	will_return(__wrap_malloc, &vdev);

	expect_assert_failure(vhid_touch_create_dev(0, NULL));
}

static void vhid_touch_create_dev_malloc_fail(void **state)
{
	struct create_msg msg = {
		.name = "ut_test",
		.data = { 0, 1920, 16, 0, 1080, 16 },

	};

	will_return(__wrap_malloc, 0);
	will_return(__wrap_malloc, NULL);
	struct vhid_dev *dev = vhid_touch_create_dev(0, &msg);
	assert_null(dev);
}

static void vhid_touch_create_dev_open_fail(void **state)
{
	struct vhid_dev vdev;
	struct create_msg msg = {
		.name = "ut_test",
		.data = { 0, 1920, 16, 0, 1080, 16 },

	};

	will_return(__wrap_malloc, 0);
	will_return(__wrap_malloc, &vdev);
	will_return(__wrap_open, -1);
	will_return(__wrap_open, ENOENT);
	will_return(__wrap_free, 1);
	will_return(__wrap_free, &vdev);

	struct vhid_dev *dev = vhid_touch_create_dev(0, &msg);
	assert_null(dev);
}

int __wrap_ioctl(int fd, unsigned long request, ...)
{
	if (fd < 0) {
		errno = EBADF;
		return -1;
	}

	check_expected(fd);

	int ret = mock_type(int);
	if (ret < 0) {
		errno = mock_type(int);
	}
	return ret;
}

static void vhid_touch_create_dev_reg_ioctl_fail(void **state)
{
	struct vhid_dev vdev;
	struct create_msg msg = {
		.name = "ut_test",
		.data = { 0, 1920, 16, 0, 1080, 16 },

	};

	will_return(__wrap_malloc, 0);
	will_return(__wrap_malloc, &vdev);
	will_return(__wrap_open, 5);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, -1);
	will_return(__wrap_ioctl, ENOTTY);
	will_return(__wrap_close, 0);
	will_return(__wrap_free, 1);
	will_return(__wrap_free, &vdev);

	struct vhid_dev *dev = vhid_touch_create_dev(0, &msg);
	assert_true(errno == ENOTTY);
	assert_null(dev);
}

static void vhid_touch_create_dev_setup_ioctl_fail(void **state)
{
	struct vhid_dev vdev;
	struct create_msg msg = {
		.name = "ut_test",
		.data = { 0, 1920, 16, 0, 1080, 16 },

	};

	will_return(__wrap_malloc, 0);
	will_return(__wrap_malloc, &vdev);
	will_return(__wrap_open, 5);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, 0);
	expect_value(__wrap_ioctl, fd, 5);
	will_return(__wrap_ioctl, -1);
	will_return(__wrap_ioctl, ENOTTY);
	will_return(__wrap_close, 0);
	will_return(__wrap_free, 1);
	will_return(__wrap_free, &vdev);

	struct vhid_dev *dev = vhid_touch_create_dev(0, &msg);
	assert_true(errno == ENOTTY);
	assert_null(dev);
}
int main(void)
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(vhid_touch_create_dev_null_msg),
		cmocka_unit_test(vhid_touch_create_dev_malloc_fail),
		cmocka_unit_test(vhid_touch_create_dev_open_fail),
		cmocka_unit_test(vhid_touch_create_dev_reg_ioctl_fail),
		cmocka_unit_test(vhid_touch_create_dev_setup_ioctl_fail),
	};

	return cmocka_run_group_tests(tests, NULL, NULL);
}
