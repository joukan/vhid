#ifndef __v_DEBUG_H__

#define __v_DEBUG_H__

#include <stdio.h>

#if defined(UNIT_TEST)
extern void mock_assert(const int result, const char *const expression,
			const char *const file, const int line);
#undef assert
#define assert(expression)                                                     \
	mock_assert((int)(expression), #expression, __FILE__, __LINE__);
#endif /* UNIT_TEST */

extern char v_log_level;

#define V_LOG_LEVEL_ERROR 1
#define V_LOG_LEVEL_WARN 32
#define V_LOG_LEVEL_INFO 64
#define V_LOG_LEVEL_DEBUG 96

#define v_dbg(m, fmt, ...)                                                     \
	((v_log_level > 95) ? fprintf(stderr, "[%s] %s-%d DEBUG: " fmt, m,     \
				      __func__, __LINE__, ##__VA_ARGS__) :     \
			      0)

#define v_moreinfo(m, fmt, ...)                                                \
	((v_log_level > 63) ? fprintf(stderr, "[%s] %s-%d INFO: " fmt, m,      \
				      __func__, __LINE__, ##__VA_ARGS__) :     \
			      0)

#define v_info(m, fmt, ...)                                                    \
	((v_log_level > 63) ?                                                  \
		 fprintf(stderr, "[%s] INFO: " fmt, m, ##__VA_ARGS__) :        \
		 0)

#define v_warn(m, fmt, ...)                                                    \
	((v_log_level > 31) ? fprintf(stderr, "[%s] %s-%d WARN: " fmt, m,      \
				      __func__, __LINE__, ##__VA_ARGS__) :     \
			      0)

#define v_err(m, fmt, ...)                                                     \
	((v_log_level > 0) ? fprintf(stderr, "[%s] %s-%d ERROR: " fmt, m,      \
				     __func__, __LINE__, ##__VA_ARGS__) :      \
			     0)

#define vhid_dbg(fmt, ...) v_dbg("vhid", fmt, ##__VA_ARGS__)
#define vhid_moreinfo(fmt, ...) v_moreinfo("vhid", fmt, ##__VA_ARGS__)
#define vhid_info(fmt, ...) v_info("vhid", fmt, ##__VA_ARGS__)
#define vhid_warn(fmt, ...) v_warn("vhid", fmt, ##__VA_ARGS__)
#define vhid_err(fmt, ...) v_err("vhid", fmt, ##__VA_ARGS__)

#define vscreen_dbg(fmt, ...) v_dbg("vscreen", fmt, ##__VA_ARGS__)
#define vscreen_moreinfo(fmt, ...) v_moreinfo("vscreen", fmt, ##__VA_ARGS__)
#define vscreen_info(fmt, ...) v_info("vscreen", fmt, ##__VA_ARGS__)
#define vscreen_warn(fmt, ...) v_warn("vscreen", fmt, ##__VA_ARGS__)
#define vscreen_err(fmt, ...) v_err("vscreen", fmt, ##__VA_ARGS__)

void v_dbg_init();

#define vhid_dbg_init v_dbg_init
#define vscreen_dbg_init v_dbg_init
#endif
