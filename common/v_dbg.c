#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "v_dbg.h"

char v_log_level = 64;

void v_dbg_init()
{
	long log_level = -1;
	char *log_level_env = getenv("V_LOG_LEVEL");

	do {
		if (!log_level_env)
			break;

		if (!strcmp(log_level_env, "debug")) {
			v_log_level = V_LOG_LEVEL_DEBUG;
			break;
		}

		if (!strcmp(log_level_env, "info")) {
			v_log_level = V_LOG_LEVEL_INFO;
			break;
		}

		if (!strcmp(log_level_env, "warn")) {
			v_log_level = V_LOG_LEVEL_WARN;
			break;
		}

		v_log_level = V_LOG_LEVEL_ERROR;

	} while (0);
}
