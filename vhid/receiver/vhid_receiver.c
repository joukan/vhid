#include <stdlib.h>
#include <stddef.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <linux/uinput.h>
#include <string.h>
#include <stdint.h>

#include <poll.h>

#include <netinet/in.h>
#include <sys/socket.h>

#include "vhid_device.h"
#include "vhid_remote_dev.h"
#include "v_dbg.h"

#define MAX_DEVICES 16
struct vhid_dev *dev_presented[MAX_DEVICES];
#define ID_START 0x1000
uint32_t globalId = ID_START;

struct vhid_client_dev {
	struct vhid_dev vd;
	int client_fd;
};

static struct vhid_dev *getdev(enum vhid_dev_type type, uint16_t id)
{
	int i;
	struct vhid_dev *dev = NULL;

	do {
		for (i = 0; i < MAX_DEVICES; i++) {
			if (!dev_presented[i])
				continue;

			vhid_dbg("check 0x%x:%d for 0x%x:%d\n",
				 dev_presented[i]->type, dev_presented[i]->id,
				 type, id);

			if ((dev_presented[i]->id) == id &&
			    (dev_presented[i]->type == type))
				break;
		}

		if (i < MAX_DEVICES)
			dev = dev_presented[i];

	} while (0);

	return dev;
}

static struct vhid_dev *getdev_byid(uint16_t id)
{
	int i;
	struct vhid_dev *dev = NULL;

	do {
		for (i = 0; i < MAX_DEVICES; i++) {
			if (!dev_presented[i])
				continue;

			if (dev_presented[i]->id == id)
				break;
		}

		if (i < MAX_DEVICES)
			dev = dev_presented[i];

	} while (0);

	return dev;
}

static int getid()
{
	uint32_t id = globalId;

	while (1) {
		if (id < ID_START)
			id = ID_START;

		if (getdev_byid(id)) {
			id++;
			continue;
		}

		break;
	}

	globalId = id + 1;
	return id;
}

#define container_of(ptr, type, member)                                        \
	({                                                                     \
		const typeof(((type *)0)->member) *__mptr = (ptr);             \
		(type *)((char *)__mptr - offsetof(type, member));             \
	})

static void remove_all_client_dev(int fd)
{
	int i;
	struct vhid_dev *dev = NULL;

	do {
		for (i = 0; i < MAX_DEVICES; i++) {
			if (!dev_presented[i])
				continue;

			struct vhid_client_dev *vcd = NULL;
			vcd = container_of(dev_presented[i],
					   struct vhid_client_dev, vd);

			if (vcd->client_fd == fd) {
				/* close uinput device */
				vhid_info("Remove device (ID=0x%x)\n",
					  vcd->vd.id);
				close(vcd->vd.fd);
				free(vcd);
				dev_presented[i] = NULL;
			}
		}

	} while (0);
}

static int get_free_slot()
{
	int i;

	for (i = 0; i < MAX_DEVICES; i++)
		if (!dev_presented[i])
			break;

	return i;
}

void emit(int fd, int type, int code, int val)
{
	struct input_event ie;

	ie.type = type;
	ie.code = code;
	ie.value = val;

	/* timestamp values below are ignored */
	ie.time.tv_sec = 0;
	ie.time.tv_usec = 0;

	write(fd, &ie, sizeof(ie));
}

int vhid_server_init()
{
	int ret = -1;
	int sockfd = -1;

	do {
		int len;
		struct sockaddr_in serveraddr, cli;

		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0) {
			vhid_err("Failed to create sock.\n");
			break;
		}

		int reuse = 1;
		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse,
			       sizeof(reuse)) < 0) {
			vhid_err("Failed to set address resue.\n");
			break;
		}

		bzero(&serveraddr, sizeof(serveraddr));

		serveraddr.sin_family = AF_INET;
		serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
		serveraddr.sin_port = htons(PORT);

		ret = bind(sockfd, (struct sockaddr *)&serveraddr,
			   sizeof(serveraddr));
		if (ret < 0) {
			vhid_err("Failed to bind address : %s (%d)\n",
				 strerror(errno), errno);
			break;
		}

		ret = listen(sockfd, 5);
		if (ret < 0) {
			vhid_err("Failed to listen address : %s (%d)\n",
				 strerror(errno), errno);
			break;
		}
		ret = sockfd;

	} while (0);

	if (ret < 0) {
		if (sockfd > 0)
			close(sockfd);
	}

	return ret;
}

int vhid_server_accept(int sockfd)
{
	int confd = -1;

	do {
		struct sockaddr_in cli;
		int len;

		len = sizeof(cli);
		confd = accept(sockfd, (struct sockaddr *)&cli,
			       (socklen_t *)&len);
		if (confd < 0)
			break;

		vhid_dbg("Connection from %s...\n", inet_ntoa(cli.sin_addr));

	} while (0);

	return confd;
}

static void vhid_do_event(struct vhid_msg *vm)
{
	do {
		int i;
		struct vhid_dev *dev;

		if (!vm)
			break;

		dev = getdev(vm->hdr.type, vm->hdr.id);
		if (!dev) {
			vhid_warn("Invalid device 0x%x:%d\n", vm->hdr.type,
				  vm->hdr.id);
			break;
		}
		for (i = 0; i < vm->hdr.num_ev; i++) {
			vhid_dbg("Emit type:%d code:%d value:%d\n",
				 vm->ev[i].type, vm->ev[i].code,
				 vm->ev[i].value);
			emit(dev->fd, vm->ev[i].type, vm->ev[i].code,
			     vm->ev[i].value);
		}

	} while (0);
}

static void vhid_do_alive(int fd, struct vhid_msg *vm)
{
	char buf[MSG_KEEPALIVE_SIZE];
	struct vhid_msg *alive = (struct vhid_msg *)buf;

	memset(alive, 0, MSG_KEEPALIVE_SIZE);
	alive->hdr.msg_type = EVT_KEEPALIVE;
	alive->hdr.resp = RESP_KEEPALIVE;
	vhid_msg_write(fd, alive);
}

static void vhid_do_create(int fd, struct vhid_msg *vm)
{
	int slot = get_free_slot();
	struct vhid_msg_hdr rsp;
	struct vhid_client_dev *vcd = malloc(sizeof(*vcd));
	struct vhid_dev *vd = NULL;

	rsp.msg_type = EVT_RESP;
	rsp.type = vm->hdr.type;
	rsp.id = vm->hdr.id;
	rsp.resp = RESP_NG;

	do {
		uint32_t id = getid();

		if (!vcd)
			break;

		if (slot == MAX_DEVICES) {
			vhid_warn("No free device slot for new device\n");
			break;
		}

		switch (vm->hdr.type) {
		case VHID_TOUCH:
			vd = vhid_touch_create_dev(id, &vm->cmsg);
			break;
		case VHID_MOUSE:
			vd = vhid_mouse_create_dev(id, &vm->cmsg);
			break;
		case VHID_KEYBOARD:
			vd = vhid_keyboard_create_dev(id, &vm->cmsg);
			break;
		default:
			break;
		}

		if (!vd)
			break;

		memcpy(&vcd->vd, vd, sizeof(struct vhid_dev));
		vhid_dev_free(vd);

		dev_presented[slot] = &vcd->vd;
		vcd->client_fd = fd;
		rsp.resp = id;

		vhid_info("Device 0x%x created at %d.\n", id, slot);
	} while (0);

	vhid_msg_write(fd, (struct vhid_msg *)&rsp);
}

#define MAX_POLL_FDS 32
#define MAX_CONNECTION (MAX_POLL_FDS - 1)

int main(void)
{
	int ret = -1;
	int touch_fd = -1;
	struct pollfd pfd[MAX_POLL_FDS];

	do {
		int sockfd = -1;
		int confd = -1;
		int i;

		vhid_dbg_init();

		sockfd = vhid_server_init();
		if (sockfd < 0)
			break;

		struct vhid_msg *vm;

		vm = vhid_msg_alloc_buf(MSG_EVENT_BODY_SIZE(UINT16_MAX));
		if (!vm)
			break;

		for (i = 0; i < MAX_POLL_FDS; i++)
			pfd[i].fd = -1;

		pfd[0].fd = sockfd;
		pfd[0].events = POLLIN;

		vhid_dbg("Enter event waiting loop ...\n");
		while (1) {
			int fd;
			struct vhid_dev *dev;

			ret = poll(pfd, MAX_POLL_FDS, -1);
			if (ret < 0) {
				vhid_err("Failed to poll: %s(%d)\n",
					 strerror(errno), errno);
				break;
			}

			if (pfd[0].revents & POLLIN) {
				vhid_dbg("Accept\n");
				fd = vhid_server_accept(sockfd);
				if (fd > 0) {
					int i;
					for (i = 0; i < MAX_POLL_FDS; i++) {
						if (pfd[i].fd == -1) {
							vhid_dbg(
								"%s-%d: register %d\n",
								__func__,
								__LINE__, fd);
							pfd[i].fd = fd;
							pfd[i].events = POLLIN;
							break;
						}
					}

					if (i >= MAX_POLL_FDS) {
						vhid_err(
							"Max connection number exceed");
						close(fd);
					}
				}
			}

			for (i = 1; i < MAX_POLL_FDS; i++) {
				if (pfd[i].revents & POLLIN) {
					vhid_dbg("Message\n");
					ret = vhid_msg_read(
						pfd[i].fd, vm,
						MSG_EVENT_SIZE(UINT16_MAX));

					if (ret <= 0) {
						vhid_dbg(
							"Error reading message: %d\n",
							ret);
						remove_all_client_dev(
							pfd[i].fd);
						close(pfd[i].fd);

						/* poll() will ignore entry
						 * whose fd is negative
						 */
						pfd[i].fd = -1;
						continue;
					}

					vhid_dbg("VHID Msg ver: %d\n",
						 vm->hdr.ver);

					switch (vm->hdr.msg_type) {
					case EVT_EVENT:
						vhid_dbg("EVT_EVETNT\n");
						vhid_do_event(vm);
						break;
					case EVT_CREATE:
						vhid_dbg("EVT_CREATE\n");
						vhid_do_create(pfd[i].fd, vm);
						break;
					case EVT_KEEPALIVE:
						vhid_dbg("EVT_KEEPALIVE\n");
						vhid_do_alive(pfd[i].fd, vm);
						break;
					default:
						vhid_warn(
							"Invalid msg received.\n");
						break;
					}
				}
			}
		}

		vhid_msg_free(vm);
		close(sockfd);
	} while (0);

	if (touch_fd > 0) {
		ioctl(touch_fd, UI_DEV_DESTROY);
		close(touch_fd);
	}

	if (!ret)
		exit(1);

	exit(0);
}
