#ifndef __VHID_DEVICES_H__
#define __VHID_DEVICES_H__
#include <stdlib.h>
#include <stdint.h>

struct input_device_config {
	unsigned long ioctl;
	unsigned long value;
};

enum vhid_dev_type {
	TOUCH_DEVICE = 0x0000,
	VHID_TOUCH = TOUCH_DEVICE,

	MOUSE_DEVICE = 0xa000,
	VHID_MOUSE = MOUSE_DEVICE,

	KEYBOARD_DEVICE = 0xc000,
	VHID_KEYBOARD = KEYBOARD_DEVICE,

	VHID_INVALID = 0xFFFF,
};

struct vhid_dev {
	enum vhid_dev_type type;
	int fd;
	uint32_t id;
	void *priv;
};

static inline const char *vhid_dev_str(struct vhid_dev *vd)
{
	if (vd) {
		switch (vd->type) {
		case VHID_TOUCH:
			return "VHID_TOUCH";
		case VHID_MOUSE:
			return "VHID_MOUSE";
		case VHID_KEYBOARD:
			return "VHID_KEYBOARD";
		default:
			break;
		}
	}

	return "VHID_INVALID";
}

static inline struct vhid_dev *vhid_dev_alloc()
{
	struct vhid_dev *vd = (struct vhid_dev *)malloc(sizeof(*vd));

	if (vd) {
		vd->type = VHID_INVALID;
		vd->id = 0;
		vd->fd = 0;
		vd->priv = NULL;
	}

	return vd;
}

static inline void vhid_dev_free(struct vhid_dev *vd)
{
	if (vd) {
		if (vd->priv)
			free(vd->priv);

		free(vd);
	}
}

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

struct create_msg;
struct vhid_dev *vhid_touch_create_dev(uint32_t id, struct create_msg *cmsg);
struct vhid_dev *vhid_mouse_create_dev(uint32_t id, struct create_msg *cmsg);
struct vhid_dev *vhid_keyboard_create_dev(uint32_t id, struct create_msg *cmsg);
#endif
