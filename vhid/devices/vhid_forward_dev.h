#ifndef __VHID_FORWARD_DEV_H__
#define __VHID_FORWARD_DEV_H__

#include <libevdev/libevdev.h>
#include "vhid_device.h"

struct vhid_forward_dev {
	struct vhid_dev vd;
	const char *devpath;
	struct libevdev *dev;
	int initialized;
};

static inline int valid_vhid_forward_dev(struct vhid_forward_dev *vfd)
{
	if (!vfd || !vfd->devpath || !vfd->dev || !vfd->initialized)
		return 0;

	return 1;
}

enum vhid_dev_type vhid_get_forward_devtype(struct libevdev *dev);
const char *vhid_get_forward_devtype_str(enum vhid_dev_type type);
void vhid_free_forward_dev(struct vhid_forward_dev *vfd);
struct vhid_forward_dev *vhid_create_forward_dev(const char *path);

#endif /* __VHID_UITLS_H__ */
