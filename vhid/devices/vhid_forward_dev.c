#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>

#include <stdint.h>
#include <libevdev/libevdev.h>

#include "vhid_device.h"
#include "vhid_forward_dev.h"
#include "v_dbg.h"

enum vhid_dev_type vhid_get_forward_devtype(struct libevdev *dev)
{
#if 0
	if (libevdev_has_event_type(dev, EV_ABS) &&
		libevdev_has_event_code(dev, EV_KEY, BTN_TOUCH) &&
		libevdev_has_property(dev, INPUT_PROP_DIRECT))
		return VHID_TOUCH;
#else
	if (libevdev_has_event_type(dev, EV_ABS) &&
	    libevdev_has_event_code(dev, EV_KEY, BTN_TOUCH))
		return VHID_TOUCH;
#endif

	if (libevdev_has_event_type(dev, EV_REL) &&
	    libevdev_has_event_code(dev, EV_KEY, BTN_LEFT))
		return VHID_MOUSE;

	if (libevdev_has_event_type(dev, EV_KEY))
		return VHID_KEYBOARD;

	return VHID_INVALID;
}

const char *vhid_get_forward_devtype_str(enum vhid_dev_type type)
{
	switch (type) {
	case VHID_TOUCH:
		return "VHID_TOUCH";
	case VHID_MOUSE:
		return "VHID_MOUSE";
	case VHID_KEYBOARD:
		return "VHID_KEYBOARD";
	default:
		break;
	}

	return "VHID_INVALID";
}

void vhid_free_forward_dev(struct vhid_forward_dev *vfd)
{
	if (!vfd)
		return;

	if (vfd->dev)
		libevdev_free(vfd->dev);

	if (vfd->vd.fd > 0)
		close(vfd->vd.fd);

	if (vfd->devpath)
		free((void *)vfd->devpath);

	if (vfd->vd.priv)
		free(vfd->vd.priv);

	free(vfd);
}

struct vhid_forward_dev *vhid_create_forward_dev(const char *path)
{
	struct vhid_forward_dev *vfd = NULL;

	do {
		int rc;

		if (!path)
			break;

		vfd = calloc(sizeof(*vfd), 1);
		if (!vfd)
			break;

		vfd->vd.id = 0;

		vfd->devpath = (const char *)strdup(path);
		if (!vfd->devpath)
			break;

		vfd->vd.fd = open(vfd->devpath, O_RDONLY);
		if (vfd->vd.fd < 0)
			break;

		rc = libevdev_new_from_fd(vfd->vd.fd, &vfd->dev);
		if (rc < 0)
			break;

		vfd->vd.type = vhid_get_forward_devtype(vfd->dev);
		if (vfd->vd.type == VHID_INVALID) {
			vhid_err("Unsupported dvevice.\n");
			break;
		}

		if (vfd->vd.type == VHID_TOUCH) {
			const struct input_absinfo *abs;
			int32_t *data;

			if (!libevdev_has_event_code(vfd->dev, EV_ABS, ABS_X)) {
				vhid_err("No ABS_X info.");
				break;
			}

			if (!libevdev_has_event_code(vfd->dev, EV_ABS, ABS_Y)) {
				vhid_err("No ABS_Y info.");
				break;
			}

			vfd->vd.priv = malloc(sizeof(int32_t) * 6);
			if (!vfd->vd.priv)
				break;

			abs = libevdev_get_abs_info(vfd->dev, ABS_X);
			data = (int32_t *)vfd->vd.priv;

			data[0] = abs->minimum;
			data[1] = abs->maximum;
			data[2] = abs->resolution ? abs->resolution : 0;

			abs = libevdev_get_abs_info(vfd->dev, ABS_Y);
			data[3] = abs->minimum;
			data[4] = abs->maximum;
			data[5] = abs->resolution ? abs->resolution : 0;
		}

		vfd->initialized = 1;

	} while (0);

	if (!valid_vhid_forward_dev(vfd)) {
		vhid_free_forward_dev(vfd);
		vfd = NULL;
	}

	return vfd;
}
