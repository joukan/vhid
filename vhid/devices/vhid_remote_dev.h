#ifndef __VHID_REMOTE_DEVICE_H__
#define __VHID_REMOTE_DEVICE_H__
#include <stdlib.h>
#include <linux/uinput.h>
#include <arpa/inet.h>
#include <stdint.h>

#include "vhid_device.h"
#include "v_dbg.h"

#define PORT 8888

struct vhid_event {
	int16_t type;
	int16_t code;
	int32_t value;
};

enum msg_type {
	EVT_RESP = 0x0000,
	EVT_KEEPALIVE = EVT_RESP,
	EVT_CREATE,
	EVT_EVENT,
	EVT_INVALID = 0xffff
};

#define RESP_NG 0
#define RESP_KEEPALIVE 1
#define IS_RESP_NG(x) (x == RESP_NG)
#define IS_RESP_OK(x) (!IS_RESP_NG(x))

struct vhid_msg_hdr {
	uint32_t ver;
	uint16_t msg_type;
	uint16_t type;
	uint32_t id;
	union {
		uint32_t num_ev;
		uint32_t body_size;
		uint32_t resp;
	};
} __attribute__((packed));

#define MAX_DEVICE_NAME_LEN 16
struct create_msg {
	char name[MAX_DEVICE_NAME_LEN];
	union {
		int32_t data[6];
	};
} __attribute__((packed));

#define VHID_MSG_VER_1 1
struct vhid_msg {
	struct vhid_msg_hdr hdr;
	union {
		/* msg of EVT_CREATE for touch_device */
		struct create_msg cmsg;
		struct vhid_event ev[0];
	};
} __attribute__((packed));

#define x_min(vm) vm->cmsg.data[0]
#define x_max(vm) vm->cmsg.data[1]
#define x_res(vm) vm->cmsg.data[2]
#define y_min(vm) vm->cmsg.data[3]
#define y_max(vm) vm->cmsg.data[4]
#define y_res(vm) vm->cmsg.data[5]

#define MSG_HDR_SIZE (sizeof(struct vhid_msg_hdr))
#define MSG_EVENT_BODY_SIZE(x) (x * sizeof(struct vhid_event))
#define MSG_EVENT_SIZE(x) (x * sizeof(struct vhid_event) + MSG_HDR_SIZE)
#define MSG_RESP_SIZE MSG_HDR_SIZE
#define MSG_KEEPALIVE_SIZE MSG_RESP_SIZE
#define MSG_REQ_CREATE_TOUCH_SIZE (MSG_HDR_SIZE + sizeof(struct create_msg))
#define MSG_REQ_CREATE_MOUSE_SIZE MSG_REQ_CREATE_TOUCH_SIZE
#define MSG_REQ_CREATE_KEYBOARD_SIZE MSG_REQ_CREATE_TOUCH_SIZE

static inline struct vhid_msg *vhid_msg_alloc_buf(int32_t extra)
{
	return (struct vhid_msg *)malloc(sizeof(struct vhid_msg_hdr) + extra);
}

static inline struct vhid_msg *vhid_msg_event(uint16_t type, uint32_t id,
					      uint32_t num_ev)
{
	struct vhid_msg *vm = vhid_msg_alloc_buf(MSG_EVENT_BODY_SIZE(num_ev));

	if (vm) {
		vm->hdr.ver = VHID_MSG_VER_1;
		vm->hdr.msg_type = EVT_EVENT;
		vm->hdr.id = id;
		vm->hdr.type = type;
		vm->hdr.num_ev = num_ev;
	}

	return vm;
}

static inline struct vhid_msg *vhid_msg_req_create(enum vhid_dev_type type)
{
	struct vhid_msg *vm = vhid_msg_alloc_buf(sizeof(struct create_msg));

	if (vm) {
		vm->hdr.ver = VHID_MSG_VER_1;
		vm->hdr.msg_type = EVT_CREATE;
		vm->hdr.type = type;
		vm->hdr.id = 0;
		vm->hdr.body_size = sizeof(struct create_msg);
	}

	return vm;
}

static inline struct vhid_msg *
vhid_msg_rsp(uint16_t id, enum vhid_dev_type type, uint32_t resp)
{
	struct vhid_msg *vm = vhid_msg_alloc_buf(0);

	if (vm) {
		vm->hdr.ver = VHID_MSG_VER_1;
		vm->hdr.msg_type = EVT_RESP;
		vm->hdr.type = type;
		vm->hdr.id = id;
		vm->hdr.resp = resp;
	}

	return vm;
}

static inline void vhid_msg_free(struct vhid_msg *vm)
{
	if (vm)
		free(vm);
}

static inline int32_t vhid_msg_len(struct vhid_msg *vm)
{
	switch (vm->hdr.msg_type) {
	case EVT_EVENT:
		return MSG_EVENT_SIZE(vm->hdr.num_ev);
	case EVT_CREATE:
		if (vm->hdr.type == VHID_TOUCH)
			return MSG_REQ_CREATE_TOUCH_SIZE;
		if (vm->hdr.type == VHID_MOUSE)
			return MSG_REQ_CREATE_MOUSE_SIZE;
		if (vm->hdr.type == VHID_KEYBOARD)
			return MSG_REQ_CREATE_KEYBOARD_SIZE;

		break;
	case EVT_RESP:
		return MSG_RESP_SIZE;
	default:
		break;
	}

	return -1;
}

static inline struct vhid_msg *vhid_msg_hton(struct vhid_msg *vm)
{
	if (vm->hdr.msg_type == EVT_EVENT) {
		int i;

		for (i = 0; i < vm->hdr.num_ev; i++) {
			vm->ev[i].type = htons(vm->ev[i].type);
			vm->ev[i].code = htons(vm->ev[i].code);
			vm->ev[i].value = htonl(vm->ev[i].value);
		}
		vm->hdr.num_ev = htonl(vm->hdr.num_ev);
	}

	if (vm->hdr.msg_type == EVT_CREATE) {
		if (vm->hdr.type == VHID_TOUCH) {
			x_min(vm) = htonl(x_min(vm));
			x_max(vm) = htonl(x_max(vm));
			x_res(vm) = htonl(x_res(vm));
			y_min(vm) = htonl(y_min(vm));
			y_max(vm) = htonl(y_max(vm));
			y_res(vm) = htonl(y_res(vm));
		}
		vm->hdr.body_size = htonl(vm->hdr.body_size);
	}

	if (vm->hdr.msg_type == EVT_RESP) {
		vm->hdr.resp = htonl(vm->hdr.resp);
	}

	vm->hdr.ver = htonl(vm->hdr.ver);
	vm->hdr.msg_type = htons(vm->hdr.msg_type);
	vm->hdr.type = htons(vm->hdr.type);
	vm->hdr.id = htonl(vm->hdr.id);

	return vm;
}

static inline struct vhid_msg *vhid_msg_ntoh(struct vhid_msg *vm)
{
	vm->hdr.ver = htonl(vm->hdr.ver);
	vm->hdr.msg_type = ntohs(vm->hdr.msg_type);
	vm->hdr.type = ntohs(vm->hdr.type);
	vm->hdr.id = htonl(vm->hdr.id);

	if (vm->hdr.msg_type == EVT_RESP) {
		vm->hdr.resp = ntohl(vm->hdr.resp);
	}

	if (vm->hdr.msg_type == EVT_EVENT) {
		int i;

		vm->hdr.num_ev = ntohl(vm->hdr.num_ev);

		for (i = 0; i < vm->hdr.num_ev; i++) {
			vm->ev[i].type = ntohs(vm->ev[i].type);
			vm->ev[i].code = ntohs(vm->ev[i].code);
			vm->ev[i].value = ntohl(vm->ev[i].value);
		}
	}

	if (vm->hdr.msg_type == EVT_CREATE) {
		if (vm->hdr.type == VHID_TOUCH) {
			x_min(vm) = ntohl(x_min(vm));
			x_max(vm) = ntohl(x_max(vm));
			x_res(vm) = ntohl(x_res(vm));
			y_min(vm) = ntohl(y_min(vm));
			y_max(vm) = ntohl(y_max(vm));
			y_res(vm) = ntohl(y_res(vm));
		}
		vm->hdr.body_size = ntohl(vm->hdr.body_size);
	}

	return vm;
}

int vhid_msg_write(int fd, struct vhid_msg *msg);
int read_bytes(int fd, char *buf, int bytes);
int vhid_msg_read(int fd, struct vhid_msg *vm, int maxsize);
int vhid_client_init(const char *serverip);
int vhid_create_remote_device(int sockfd, struct vhid_dev *vd, char *name);
#endif
