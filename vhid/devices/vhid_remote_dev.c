#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include "vhid_remote_dev.h"
#include "v_dbg.h"

int vhid_msg_write(int fd, struct vhid_msg *vm)
{
	int ret = -1;

	do {
		if (fd < 0 || !vm)
			break;

		int len = (int)vhid_msg_len(vm);

#if 0
		vhid_dbg("Write vhid_msg_hdr of %lu bytes\n",
				(unsigned long)sizeof(struct vhid_msg_hdr));

		vhid_dbg("Write %d events of %d bytes\n",
				vm->hdr.num_ev,
				vhid_msg_len(vm));
#endif
		ret = write(fd, vhid_msg_hton(vm), len);

	} while (0);

	return ret;
}

int read_bytes(int fd, char *buf, int bytes)
{
	int readbytes = 0;
	int ret = -1;

	do {
		int len;

		ret = read(fd, buf + readbytes, bytes - readbytes);
		if (ret <= 0) {
			if ((ret < 0) && (errno == EAGAIN))
				continue;

			if (ret < 0)
				vhid_err("read err (%d): %s\n", errno,
					 strerror(errno));
			else
				vhid_err("read reachs end.\n");
			break;
		}

		readbytes += ret;

	} while (readbytes < bytes);

	return (readbytes == bytes) ? readbytes : ret;
}

int vhid_msg_read(int fd, struct vhid_msg *vm, int maxsize)
{
	int ret = -1;

	do {
		int readbytes;
		uint16_t msg_type;
		uint16_t type;
		int len;
		int32_t max_body_size = maxsize - sizeof(struct vhid_msg_hdr);
		uint32_t body_size = 0;

		assert(vm);
		assert(max_body_size >= 0);

		ret = read_bytes(fd, (char *)&vm->hdr,
				 sizeof(struct vhid_msg_hdr));

		if (ret <= 0)
			break;

		vhid_dbg("%s - %d vhid_msg hdr size: %d\n", __func__, __LINE__,
			 ret);

		msg_type = ntohs(vm->hdr.msg_type);
		/* EVT_KEEPLIVE == EVT_RESP */
		if (msg_type == EVT_RESP)
			break;

		if (msg_type == EVT_CREATE) {
			body_size = ntohl(vm->hdr.body_size);
			vhid_dbg("%s - %d vhid_msg body size: %d\n", __func__,
				 __LINE__, body_size);
			assert(max_body_size >= body_size);
			ret = read_bytes(fd, (char *)&vm->cmsg, body_size);
			break;
		}

		if (msg_type == EVT_EVENT) {
			uint32_t num_ev = ntohl(vm->hdr.num_ev);

			vhid_dbg("ntohl(vm->hdr.num_ev):  %d -> %d\n",
				 vm->hdr.num_ev, num_ev);

			body_size = sizeof(struct vhid_event) * num_ev;
			assert(max_body_size >= body_size);
			ret = read_bytes(fd, (char *)&vm->ev, body_size);
			break;
		}

		ret = -1;

	} while (0);

	if (ret > 0)
		vhid_msg_ntoh(vm);

	return ret > 0 ? vhid_msg_len(vm) : ret;
}

int vhid_client_init(const char *serverip)
{
	int sockfd = -1;

	do {
		int ret;
		struct sockaddr_in serveraddr;

		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0)
			break;

		bzero(&serveraddr, sizeof(serveraddr));

		serveraddr.sin_family = AF_INET;
		serveraddr.sin_addr.s_addr = inet_addr(serverip);
		serveraddr.sin_port = htons(PORT);

		vhid_dbg("Conect to %s\n", inet_ntoa(serveraddr.sin_addr));

		ret = connect(sockfd, (struct sockaddr *)&serveraddr,
			      sizeof(serveraddr));

		if (ret < 0) {
			vhid_dbg("Failed to connect(%d): %s\n", errno,
				 strerror(errno));
			close(sockfd);
			sockfd = -1;
		} else {
			vhid_info("Conected VHID server(%s)\n",
				  inet_ntoa(serveraddr.sin_addr));
		}

	} while (0);

	return sockfd;
}

int vhid_create_remote_device(int sockfd, struct vhid_dev *vd, char *name)
{
	int ret = -1;
	struct vhid_msg *vmsg = NULL;

	do {
		int i;
		char buf[MSG_RESP_SIZE];

		vmsg = vhid_msg_req_create(vd->type);
		if (!vmsg)
			break;

		if (name) {
			strncpy(vmsg->cmsg.name, name, MAX_DEVICE_NAME_LEN);
			vmsg->cmsg.name[MAX_DEVICE_NAME_LEN - 1] = '\0';
		} else {
			strcpy(vmsg->cmsg.name, "dev");
		}

		if (vd->type == VHID_TOUCH) {
			int32_t *data = vd->priv;

			for (i = 0; i < 6; i++)
				vmsg->cmsg.data[i] = data[i];
		}

		ret = vhid_msg_write(sockfd, vmsg);
		if (ret < 0)
			break;

		/* Reuse vmsg to receive response */
		ret = vhid_msg_read(sockfd, (struct vhid_msg *)buf,
				    MSG_RESP_SIZE);

		if (ret > 0) {
			struct vhid_msg *rsp = (struct vhid_msg *)buf;

			if ((rsp->hdr.msg_type == EVT_RESP) &&
			    IS_RESP_OK(rsp->hdr.resp)) {
				vhid_info(
					"remote %s devcie created (ID=0x%x).\n",
					vhid_dev_str(vd), rsp->hdr.resp);
				vd->id = rsp->hdr.resp;
				ret = 0;
				break;
			}
		}

		ret = -1;
	} while (0);

	if (vmsg)
		vhid_msg_free(vmsg);

	return ret;
}
