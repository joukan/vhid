#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <linux/uinput.h>
#include "vhid_remote_dev.h"
#include "v_dbg.h"

static struct input_device_config configs[] = {
	/* mouse buttons */
	{ UI_SET_EVBIT, EV_KEY },
	{ UI_SET_KEYBIT, BTN_LEFT },
	{ UI_SET_KEYBIT, BTN_RIGHT },
	{ UI_SET_KEYBIT, BTN_MIDDLE },
	/* mouse movements */
	{ UI_SET_EVBIT, EV_REL },
	{ UI_SET_RELBIT, REL_X },
	{ UI_SET_RELBIT, REL_Y },
	/* wheel */
	{ UI_SET_RELBIT, REL_WHEEL },
	{ UI_SET_RELBIT, REL_HWHEEL },
};

struct vhid_dev *vhid_mouse_create_dev(uint32_t id, struct create_msg *cmsg)
{
	int ret = -1;
	struct vhid_dev *dev = malloc(sizeof(*dev));

	do {
		struct uinput_setup usetup;
		int i;

		if (!dev)
			break;

		dev->type = VHID_MOUSE;
		dev->fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
		dev->id = id;
		if (dev->fd < 0) {
			vhid_err("Open /dev/uinput failed (%d): %s\n", errno,
				 strerror(errno));
			break;
		}

		/* Register function keys */
		for (i = 0; i < ARRAY_SIZE(configs); i++) {
			ret = ioctl(dev->fd, configs[i].ioctl,
				    configs[i].value);
			if (ret < 0) {
				vhid_err("config device failed: (%d): %s\n",
					 errno, strerror(errno));
				break;
			}
		}

		memset(&usetup, 0, sizeof(usetup));
		usetup.id.bustype = BUS_VIRTUAL;
		usetup.id.vendor = 0x5000;
		usetup.id.product = 0x0001;
		sprintf(usetup.name, "vhid_mouse-%s", cmsg->name);

		ret = ioctl(dev->fd, UI_DEV_SETUP, &usetup);
		if (ret < 0) {
			vhid_err("UI_DEV_SETUP failed: (%d): %s\n", errno,
				 strerror(errno));
			break;
		}

		ret = ioctl(dev->fd, UI_DEV_CREATE);
	} while (0);

	if (ret < 0 && dev) {
		if (dev->fd > 0)
			close(dev->fd);
		free(dev);
		dev = NULL;
	}

	return dev;
}
