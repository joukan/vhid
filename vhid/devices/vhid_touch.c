#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <linux/uinput.h>
#include "vhid_remote_dev.h"
#include "v_dbg.h"

static struct input_device_config configs[] = {
	/* Touch Button */
	{ UI_SET_EVBIT, EV_KEY },
	{ UI_SET_KEYBIT, BTN_TOUCH },

	/* ABS Movement */
	{ UI_SET_EVBIT, EV_ABS },
	{ UI_SET_PROPBIT, INPUT_PROP_DIRECT },
};

struct vhid_dev *vhid_touch_create_dev(uint32_t id, struct create_msg *cmsg)
{
	int ret = -1;
	struct vhid_dev *dev = malloc(sizeof(*dev));
	assert(cmsg);

	do {
		struct uinput_setup usetup;
		int i;
		struct uinput_abs_setup axis[2];

		if (!dev)
			break;

		dev->type = VHID_TOUCH;
		dev->fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
		dev->id = id;
		if (dev->fd < 0) {
			vhid_err("Open /dev/uinput failed (%d): %s\n", errno,
				 strerror(errno));
			break;
		}

		/* Register function keys */
		for (i = 0; i < ARRAY_SIZE(configs); i++) {
			ret = ioctl(dev->fd, configs[i].ioctl,
				    configs[i].value);
			if (ret < 0)
				break;
		}

		if (ret < 0)
			break;

		memset(axis, 0, sizeof(axis));
		axis[0].code = ABS_X;
		axis[0].absinfo.minimum = cmsg->data[0];
		axis[0].absinfo.maximum = cmsg->data[1];
		axis[0].absinfo.resolution = cmsg->data[2];

		axis[1].code = ABS_Y;
		axis[1].absinfo.minimum = cmsg->data[3];
		axis[1].absinfo.maximum = cmsg->data[4];
		axis[1].absinfo.resolution = cmsg->data[5];

		for (i = 0; i < ARRAY_SIZE(axis); i++) {
			ret = ioctl(dev->fd, UI_ABS_SETUP, &axis[i]);
			if (ret < 0)
				break;
		}

		if (ret < 0)
			break;

		memset(&usetup, 0, sizeof(usetup));
		usetup.id.bustype = BUS_VIRTUAL;
		usetup.id.vendor = 0x0001;
		usetup.id.product = 0x0002;
		sprintf(usetup.name, "vhid_touch-%s", cmsg->name);

		ret = ioctl(dev->fd, UI_DEV_SETUP, &usetup);
		if (ret < 0)
			break;

		ret = ioctl(dev->fd, UI_DEV_CREATE);
	} while (0);

	if (ret < 0 && dev) {
		if (dev->fd > 0)
			close(dev->fd);
		free(dev);
		dev = NULL;
	}

	return dev;
}
