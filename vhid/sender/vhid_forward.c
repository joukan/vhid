#include <stdio.h>
#include <stdlib.h>
#include <libevdev/libevdev.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include "vhid_forward_dev.h"
#include "vhid_remote_dev.h"
#include "v_dbg.h"

enum { ORIGIN_TOPLEFT,
       ORIGIN_TOPRIGHT,
       ORIGIN_BOTTOMLEFT,
       ORIGIN_BOTTOMRIGHT,
};

static inline int32_t trans(struct input_event *ie, int in_origin,
			    struct vhid_forward_dev *vfd)
{
	int32_t ret = ie->value;

	do {
		if (vfd->vd.type != TOUCH_DEVICE)
			break;

		if (in_origin == ORIGIN_TOPLEFT)
			break;

		int32_t *absinfo = (int32_t *)vfd->vd.priv;
		int32_t x_max = absinfo[1];
		int32_t y_max = absinfo[4];

		if (in_origin == ORIGIN_TOPRIGHT) {
			if (ie->code == ABS_X)
				ret = x_max - ie->value;

			break;
		}

		if (in_origin == ORIGIN_BOTTOMLEFT) {
			if (ie->code == ABS_Y)
				ret = y_max - ie->value;

			break;
		}

		if (in_origin == ORIGIN_BOTTOMRIGHT) {
			if (ie->code == ABS_X)
				ret = x_max - ie->value;

			if (ie->code == ABS_Y)
				ret = y_max - ie->value;

			break;
		}

	} while (0);

	return ret;
}

int main(int argc, char *argv[])
{
	int ret = 1;
	int sockfd = -1;
	struct vhid_forward_dev *vfd = NULL;
	int touchscreen_origin = ORIGIN_TOPLEFT;
	char *origin_pos = NULL;

	do {
		if (argc < 3) {
			printf("Usage: %s <input dev path> <user ip>\n",
			       argv[0]);
			break;
		}

		vhid_dbg_init();

		vfd = vhid_create_forward_dev(argv[1]);
		if (!vfd)
			break;

		if (vfd->vd.type == VHID_TOUCH) {
			origin_pos = getenv("TOUCH_SCREEN_ORIGIN");
			if (origin_pos) {
				if (!strcmp(origin_pos, "topleft")) {
					touchscreen_origin = ORIGIN_TOPLEFT;
				} else if (!strcmp(origin_pos, "topright")) {
					touchscreen_origin = ORIGIN_TOPRIGHT;
				} else if (!strcmp(origin_pos, "bottomleft")) {
					touchscreen_origin = ORIGIN_BOTTOMLEFT;
				} else if (!strcmp(origin_pos, "bottomright")) {
					touchscreen_origin = ORIGIN_BOTTOMRIGHT;
				} else {
					vhid_warn(
						"TOUCH_SCREEN_ORIGIN(=%s) invalid, Use default setting.\n",
						origin_pos);
					origin_pos = "topleft";
				}
			}

			vhid_info("Input device origin is %s (%d)\n",
				  origin_pos, touchscreen_origin);
		}

		sockfd = vhid_client_init(argv[2]);
		if (sockfd < 0)
			break;

		ret = vhid_create_remote_device(sockfd, (struct vhid_dev *)vfd, "fwd");
		if (ret < 0)
			break;

		uint32_t num_ev = 0;
		while (1) {
			int rc;
			struct input_event ie[UINT16_MAX];
			struct input_event ev;

			rc = libevdev_next_event(
				vfd->dev,
				LIBEVDEV_READ_FLAG_NORMAL |
					LIBEVDEV_READ_FLAG_BLOCKING,
				&ie[num_ev++]);

			if (rc == LIBEVDEV_READ_STATUS_SYNC) {
				do {
					rc = libevdev_next_event(
						vfd->dev,
						LIBEVDEV_READ_FLAG_SYNC,
						&ie[num_ev - 1]);

				} while (rc == LIBEVDEV_READ_STATUS_SYNC);
			}

			if (rc == -EAGAIN) {
				num_ev--;
				continue;
			}

			if (rc != LIBEVDEV_READ_STATUS_SUCCESS)
				break;

			/* Send to user */
			if (ie[num_ev - 1].type == EV_SYN) {
				int i;
				struct vhid_msg *vm;

				vm = vhid_msg_event(vfd->vd.type, vfd->vd.id,
						    num_ev);

				if (!vm)
					break;

				for (i = 0; i < num_ev; i++) {
					vm->ev[i].type = ie[i].type;
					vm->ev[i].code = ie[i].code;
					vm->ev[i].value =
						trans(&ie[i],
						      touchscreen_origin, vfd);
				}

				vhid_dbg("Send %d events\n", num_ev);
				ret = vhid_msg_write(sockfd, vm);
				vhid_msg_free(vm);

				if (ret < 0)
					break;

				num_ev = 0;
			}
		}

	} while (0);

	if (sockfd > 0)
		close(sockfd);

	if (vfd)
		vhid_free_forward_dev(vfd);

	exit(ret);
}
