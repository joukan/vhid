set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)

SET(CMAKE_C_COMPILER $ENV{JS_TOOLCHAIN}/x86_64-poky-linux/x86_64-poky-linux-gcc)
SET(CMAKE_CXX_COMPILER $ENV{JS_TOOLCHAIN}/x86_64-poky-linux/x86_64-poky-linux-g++)

# where is the target environment
execute_process(COMMAND ${CMAKE_C_COMPILER} --print-sysroot OUTPUT_VARIABLE SYSROOT)
SET(CMAKE_FIND_ROOT_PATH  ${SYSROOT} $ENV{JS_NFSROOT})

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
