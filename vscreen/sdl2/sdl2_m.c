#include "SDL.h"
#include "SDL_render.h"
#include "SDL_video.h"
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <semaphore.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <linux/uinput.h>

#include <time.h>
#include <string.h>
#include <pthread.h>

#include "sdl2_m.h"
#include "v_dbg.h"
#include "vhid_remote_dev.h"
#include "keymap.h"

#define EVENT_BUFFER 64

/* Event used in this file
	#define EV_ABS	0x03
	#define ABS_X	0x00
	#define ABS_Y	0x01

	#define EV_KEY	0x03
	#define BTN_TOUCH 0x14a

	#define EV_SYN	0x0
	#define SYN_REPORT 0x0
*/

#define TOUCH_UP 0
#define TOUCH_DOWN 1

struct vhid_remote;

struct ve_ctl {
	struct vhid_dev vd;
	struct vhid_remote *remote;
	struct vhid_event ve[EVENT_BUFFER];
	int32_t num;
	int16_t last_x;
	int16_t last_y;
	int16_t last_btn;
	char need_sync;
};

struct vhid_remote {
	const char *serverip;
	int sockfd;
	pthread_t worker;
	pthread_mutex_t mutex;
	char worker_exit;
	struct ve_ctl *vc_touch;
	struct ve_ctl *vc_keyboard;
	struct ve_ctl *vc_mouse;
	void *touch_info;
	char *vhid_dev_name;
};

static void do_sync_event(struct ve_ctl *vc)
{
	struct vhid_msg *vm = NULL;
	int ret;

	do {
		int i;

		if (!vc)
			break;

		if (!vc->need_sync)
			break;

		vm = vhid_msg_event(vc->vd.type, vc->vd.id, vc->num + 1);
		assert(vm);
		for (i = 0; i < vc->num; i++) {
			vm->ev[i].type = vc->ve[i].type;
			vm->ev[i].code = vc->ve[i].code;
			vm->ev[i].value = vc->ve[i].value;
		}

		vm->ev[i].type = EV_SYN;
		vm->ev[i].code = SYN_REPORT;
		vm->ev[i].value = 0;

		vscreen_dbg("Send %d events\n", vc->num);
		ret = vhid_msg_write(vc->remote->sockfd, vm);
		vhid_msg_free(vm);

		vc->num = 0;
		vc->need_sync = 0;

	} while (0);
}

static void ve_add_new(struct ve_ctl *vc, int16_t type, int16_t code,
		       int16_t value)
{
	if (!vc)
		return;

	do {
		if ((type != EV_KEY) && (type != EV_ABS) && (type != EV_REL))
			break;

		if (type == EV_ABS) {
			if ((code == ABS_X) && (value == vc->last_x))
				break;
			if ((code == ABS_Y) && (value == vc->last_y)) {
				break;
			}
		}

		if (type == EV_KEY) {
			if ((code == BTN_TOUCH) && (value == vc->last_btn))
				break;
		}

		if (vc->num >= EVENT_BUFFER) {
			vscreen_warn("Event buffer overflow.");
			vc->need_sync = 1;
			break;
		}

		/* for touch devcie there is no untouched movement */
		if ((type == EV_ABS) && (vc->last_btn == TOUCH_UP))
			break;

		vscreen_dbg("type: %d code %d value %x \n", type, code, value);

		vc->ve[vc->num].type = type;
		vc->ve[vc->num].code = code;
		vc->ve[vc->num].value = value;
		vc->num++;
		vc->need_sync = 1;

		vscreen_dbg("VHID event number %d \n", vc->num);

		if (type == EV_ABS) {
			if (code == ABS_X)
				vc->last_x = value;

			if (code == ABS_Y)
				vc->last_y = value;
		} else if (type == EV_KEY) {
			if (code == BTN_TOUCH)
				vc->last_btn = value;
		}

	} while (0);
}

static struct ve_ctl *ve_create(enum vhid_dev_type type,
				struct vhid_remote *remote, void *priv)
{
	struct ve_ctl *vc = NULL;
	int ret = -1;

	do {
		vc = malloc(sizeof(*vc));
		if (!vc)
			break;

		memset(vc, 0, sizeof(*vc));

		vc->vd.type = type;
		vc->vd.priv = priv;
		vc->remote = remote;

		ret = 0;
	} while (0);

	if (ret < 0) {
		if (vc) {
			free(vc);
			vc = NULL;
		}
	}

	return vc;
}

static void ve_destroy(struct ve_ctl *vc)
{
	do {
		if (!vc)
			break;

		if (vc->vd.priv)
			free(vc->vd.priv);
		free(vc);

	} while (0);
}

struct sdl2_ctl {
	SDL_Window *window;
	SDL_Renderer *render;
	SDL_Texture *texture;

	/* The size of the draw area in the texture */
	SDL_Rect rect_src;

	/* The size of the draw area before rotation */
	SDL_Rect rect_dst;
	enum rotate_drgee rotate;
	struct vhid_remote remote;
	int32_t flag;
};

#define container_of(ptr, type, member)                                        \
	({                                                                     \
		const typeof(((type *)0)->member) *__mptr = (ptr);             \
		(type *)((char *)__mptr - offsetof(type, member));             \
	})

struct sdl2_frame current_frame;
pthread_mutex_t frame_mutex = PTHREAD_MUTEX_INITIALIZER;

void sdl_update_frame(void *frame, int length)
{
	pthread_mutex_lock(&frame_mutex);
	current_frame.frame = frame;
	current_frame.length = length;
	pthread_mutex_unlock(&frame_mutex);
}

static void sdl_error(char *note)
{
	const char *error = SDL_GetError();
	if (*error != '\0') {
		vscreen_err("%s failed: %s\n", note, error);
		SDL_ClearError();
	}
}

static void *ve_worker(void *arg)
{
	struct vhid_remote *remote = arg;

	assert(remote);

	do {
		int ret;

		vscreen_info("Try to connect VHID server(%s)...\n",
			     remote->serverip);

		while (!remote->worker_exit) {
			remote->sockfd = vhid_client_init(remote->serverip);
			if (remote->sockfd > 0)
				break;

			usleep(1000 * 1000);
		}

		if (remote->worker_exit)
			break;

		struct ve_ctl *vc;
		int32_t *data;
		struct sdl2_ctl *ctl =
			container_of(remote, struct sdl2_ctl, remote);

		/* Create remote touchscreen */
		data = malloc(sizeof(int32_t) * 6);
		assert(data);
		data[0] = 0;
		data[1] = ctl->rect_dst.w;
		data[2] = 0;

		data[3] = 0;
		data[4] = ctl->rect_dst.h;
		data[5] = 0;

		vc = ve_create(VHID_TOUCH, remote, (void *)data);
		assert(vc);

		vhid_dbg("Begin create remote touch device\n");
		ret = vhid_create_remote_device(remote->sockfd,
						(struct vhid_dev *)vc,
						remote->vhid_dev_name);

		if (ret < 0) {
			vscreen_err("Failed to create remote touch device.\n");
			ve_destroy(vc);
			vc = NULL;
		} else {
			vhid_info("Remote touch device created\n");
		}

		pthread_mutex_lock(&remote->mutex);
		remote->vc_touch = vc;
		pthread_mutex_unlock(&remote->mutex);

		/* Create remote mouse */
		vhid_dbg("Begin create remote mouse device\n");
		vc = ve_create(VHID_MOUSE, remote, NULL);
		assert(vc);
		ret = vhid_create_remote_device(remote->sockfd,
						(struct vhid_dev *)vc,
						remote->vhid_dev_name);

		if (ret < 0) {
			vscreen_err("Failed to create remote mouse device.\n");
			ve_destroy(vc);
			vc = NULL;
		} else {
			vhid_info("Remote mouse device created\n");
		}

		pthread_mutex_lock(&remote->mutex);
		remote->vc_mouse = vc;
		pthread_mutex_unlock(&remote->mutex);

		/* Create remote keyboard */
		vhid_dbg("Begin create remote keyboard device\n");
		vc = ve_create(VHID_KEYBOARD, remote, NULL);
		assert(vc);
		ret = vhid_create_remote_device(remote->sockfd,
						(struct vhid_dev *)vc,
						remote->vhid_dev_name);

		if (ret < 0) {
			vscreen_err(
				"Failed to create remote keyboard device.\n");
			ve_destroy(vc);
			vc = NULL;
		} else {
			vhid_info("Remote keyboard device created\n");
		}

		pthread_mutex_lock(&remote->mutex);
		remote->vc_keyboard = vc;
		pthread_mutex_unlock(&remote->mutex);

		while (!remote->worker_exit) {
			char buf[MSG_KEEPALIVE_SIZE];
			struct vhid_msg *alive = (struct vhid_msg *)buf;
			fd_set fds;
			struct timeval to;
			int connection_lost;

			memset(alive, 0, MSG_KEEPALIVE_SIZE);
			alive->hdr.ver = VHID_MSG_VER_1;
			alive->hdr.msg_type = EVT_KEEPALIVE;
			alive->hdr.resp = EVT_INVALID;
			ret = vhid_msg_write(remote->sockfd, alive);
			assert(ret >= 0);

			to.tv_sec = 1;
			to.tv_usec = 0;

			connection_lost = 0;

			do {
				FD_ZERO(&fds);
				FD_SET(remote->sockfd, &fds);

				ret = select(remote->sockfd + 1, &fds, NULL,
					     NULL, &to);
				assert(ret >= 0);

				/* time out */
				if (ret == 0) {
					connection_lost = 1;
					break;
				}

				if (FD_ISSET(remote->sockfd, &fds)) {
					ret = vhid_msg_read(remote->sockfd,
							    alive,
							    MSG_KEEPALIVE_SIZE);

					if (ret <= 0) {
						connection_lost = 1;
						break;
					}

					/* received KEEPALIVE message */
					if ((alive->hdr.msg_type ==
					     EVT_KEEPALIVE) &&
					    (alive->hdr.resp == RESP_KEEPALIVE))
						break;

					vscreen_warn(
						"Suspicious messgage (msg_type=%x).\n",
						alive->hdr.msg_type);
				}

			} while (!remote->worker_exit);

			if (connection_lost) {
				close(remote->sockfd);
				vscreen_err("Connection to %s lost.\n",
					    remote->serverip);
				break;
			}

			sleep(1);
		}

		pthread_mutex_lock(&remote->mutex);
		ve_destroy(remote->vc_touch);
		remote->vc_touch = NULL;
		ve_destroy(remote->vc_mouse);
		remote->vc_mouse = NULL;
		ve_destroy(remote->vc_keyboard);
		remote->vc_keyboard = NULL;
		pthread_mutex_unlock(&remote->mutex);

	} while (!remote->worker_exit);

	return NULL;
}

#define ctl2handler(ctl) (ctl ? (SDL2_HANDLER)ctl : SDL2_INVALID_HANDLER)
#define handler2ctl(handler) (struct sdl2_ctl *)handler

#define MAX_NAME_LEN 128
SDL2_HANDLER sdl2_create(struct sdl2_para *para)
{
	struct sdl2_ctl *ctl;
	int ret = -1;
	char window_title[MAX_NAME_LEN + 1];

	do {
		uint32_t w = 0, h = 0;

		ctl = malloc(sizeof(*ctl));
		if (!ctl)
			break;

		memset(ctl, 0, sizeof(*ctl));

		ret = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
		if (ret != 0)
			break;

		assert(para->dev);

		/*
		 * Adjust window size for rotation.
		 */
		ctl->rotate = para->rotate;
		switch (ctl->rotate) {
		case ROTATE_0:
		case ROTATE_180:
			w = para->dst_w;
			h = para->dst_h;
			break;
		case ROTATE_90:
		case ROTATE_270:
			w = para->dst_h;
			h = para->dst_w;
			break;
		}

		assert(w != 0);
		assert(h != 0);

		snprintf(window_title, MAX_NAME_LEN, "VHID screen (%s)",
			 para->dev);
		ctl->window =
			SDL_CreateWindow(window_title, SDL_WINDOWPOS_UNDEFINED,
					 SDL_WINDOWPOS_UNDEFINED, w, h,
					 SDL_WINDOW_SHOWN);

		if (!ctl->window) {
			sdl_error("SDL_CreateWindow");
			vscreen_err("Failed to create SDL window.\n");
			break;
		}

		if (para->flag & FLG_USE_SOFTWARE_RENDER) {
			vscreen_info("Use software render fallback.");
			ctl->render = SDL_CreateRenderer(
				ctl->window, -1,
				SDL_RENDERER_TARGETTEXTURE |
					SDL_RENDERER_SOFTWARE);
		} else {
			ctl->render = SDL_CreateRenderer(
				ctl->window, -1,
				SDL_RENDERER_TARGETTEXTURE |
					SDL_RENDERER_ACCELERATED |
					SDL_RENDERER_PRESENTVSYNC);
		}

		if (!ctl->render) {
			sdl_error("SDL_CreateRenderer");
			vscreen_err("Failed to create SDL render.\n");
			SDL_DestroyWindow(ctl->window);
			SDL_Quit();
			break;
		}

		ctl->texture =
			SDL_CreateTexture(ctl->render, SDL_PIXELFORMAT_YUY2,
					  SDL_TEXTUREACCESS_STREAMING,
					  para->src_w, para->src_h);

		if (!ctl->texture) {
			SDL_DestroyRenderer(ctl->render);
			SDL_DestroyWindow(ctl->window);
			SDL_Quit();
			vscreen_err("Failed to create SDL texture.\n");
			break;
		}

		ctl->rect_src.w = para->src_w;
		ctl->rect_src.h = para->src_h;
		ctl->rect_dst.w = para->dst_w;
		ctl->rect_dst.h = para->dst_h;
		ctl->flag = para->flag;

		if (para->serverip) {
			ctl->remote.serverip = para->serverip;
			ctl->remote.vhid_dev_name = para->vhid_dev_name;

			pthread_mutex_init(&ctl->remote.mutex, NULL);
			ret = pthread_create(&ctl->remote.worker, NULL,
					     ve_worker, &ctl->remote);
		}

		ret = 0;

	} while (0);

	return ctl2handler(ctl);
}

void log_rect(char *note, SDL_Rect *rect)
{
	vscreen_dbg("%s: %dx%d@(%d,%d)\n", note, rect->w, rect->h, rect->x,
		    rect->y);
}

/*
 * Draw function
 *
 * Note this function must be called within the same thread which creates SDL
 * window.
 */
static void sdl2_draw_frame(struct sdl2_ctl *ctl)
{
	pthread_mutex_lock(&frame_mutex);
	do {
		SDL_Rect dst;
		SDL_Point c;
		double rotate_degree;
		SDL_RendererFlip flip;

		if (!ctl | !ctl->window | !ctl->render | !ctl->texture | !current_frame.frame)
			break;

		log_rect("src rect", &ctl->rect_src);
		log_rect("dst rect", &ctl->rect_dst);

		SDL_UpdateTexture(ctl->texture, &ctl->rect_src,
				  current_frame.frame, ctl->rect_src.w * 2);

		SDL_RenderClear(ctl->render);

		/*
		 * Behavior of SDL_RenderCopyEx()
		 *
		 * 1. Copy texture to destination rect and ajust size to fit it.
		 * 2. Rotate destination rect.
		 *
		 */

		switch (ctl->rotate) {
		case ROTATE_0:
			c.x = dst.w / 2;
			c.y = dst.h / 2;

			dst.x = 0;
			dst.y = 0;
			dst.w = ctl->rect_dst.w;
			dst.h = ctl->rect_dst.h;
			rotate_degree = 0.0;
			flip = SDL_FLIP_NONE;
			break;

		case ROTATE_90:
			c.x = 0;
			c.y = dst.h;

			dst.x = 0;
			dst.y = -1 * ctl->rect_dst.h;
			dst.w = ctl->rect_dst.w;
			dst.h = ctl->rect_dst.h;
			rotate_degree = 90.0;
			flip = SDL_FLIP_NONE;
			break;

		case ROTATE_180:
			c.x = dst.w / 2;
			c.y = dst.h / 2;
			dst.x = 0;
			dst.y = 0;
			dst.w = ctl->rect_dst.w;
			dst.h = ctl->rect_dst.h;
			rotate_degree = 180.0;
			flip = SDL_FLIP_NONE;
			break;

		case ROTATE_270:
			c.x = 0;
			c.y = 0;

			dst.x = 0;
			dst.y = ctl->rect_dst.w;
			dst.w = ctl->rect_dst.w;
			dst.h = ctl->rect_dst.h;
			rotate_degree = 270.0;
			flip = SDL_FLIP_NONE;
			break;
		}

		SDL_RenderCopyEx(ctl->render, ctl->texture, &ctl->rect_src,
				 &dst, rotate_degree, &c, flip);
		SDL_RenderPresent(ctl->render);

		sdl_error("SDL_RenderPresent");

	} while (0);
	pthread_mutex_unlock(&frame_mutex);
}

static void switch_mouse_mode()
{
	int ret;

	do {
		if (SDL_GetRelativeMouseMode() == SDL_TRUE) {
			vscreen_info("Mouse Relative Mode ON.\n");
			ret = SDL_SetRelativeMouseMode(SDL_FALSE);
			if (ret == 0)
				vscreen_info("Set Mouse Relative Mode OFF.\n");
			break;
		}

		vscreen_info("Mouse Relative Mode OFF.\n");
		ret = SDL_SetRelativeMouseMode(SDL_TRUE);
		if (ret == 0)
			vscreen_info("Set Mouse Relative Mode ON.\n");

	} while (0);

	if (ret < 0)
		vscreen_err("SDL_SetRelativeMouseMode failed: %s.\n",
			    SDL_GetError());
}

struct trans_para {
	int16_t x;
	int16_t y;
	int16_t is_abs;
};

void trans(struct sdl2_ctl *ctl, struct trans_para *para)
{
	int16_t ox = para->x;
	int16_t oy = para->y;

	do {
		/* target origin is at top left */
		if (ctl->rotate == ROTATE_0)
			break;

		/* target origin is at top right */
		if (ctl->rotate == ROTATE_90) {
			if (para->is_abs) {
				para->x = oy;
				para->y = (ctl->rect_dst.h - ox);
			} else {
				if (ctl->flag & FLG_KEEP_RELXY) {
					para->x = ox;
					para->y = oy;
				} else {
					para->x = oy;
					para->y = -1 * ox;
				}
			}
			break;
		}

		/* target origin is at down right */
		if (ctl->rotate == ROTATE_180) {
			if (para->is_abs) {
				para->x = (ctl->rect_dst.w - ox);
				para->y = (ctl->rect_dst.h - oy);
			} else {
				if (ctl->flag & FLG_KEEP_RELXY) {
					para->x = ox;
					para->y = oy;
				} else {
					para->x = -1 * ox;
					para->y = -1 * oy;
				}
			}
			break;
		}

		/* target origin is at down left */
		if (ctl->rotate == ROTATE_270) {
			if (para->is_abs) {
				para->x = (ctl->rect_dst.w - oy);
				para->y = ox;
			} else {
				if (ctl->flag & FLG_KEEP_RELXY) {
					para->x = ox;
					para->y = oy;
				} else {
					para->x = -1 * oy;
					para->y = ox;
				}
			}
			break;
		}

	} while (0);
}

void sdl2_event_loop(SDL2_HANDLER handler)
{
	do {
		SDL_Event e;
		int should_quit = 0;
		struct sdl2_ctl *ctl = handler2ctl(handler);
		int ret = 0;
		uint32_t keycode;
		struct trans_para tp;

		if (!ctl)
			break;

		while (!should_quit) {
			while (SDL_PollEvent(&e)) {
				switch (e.type) {
				case SDL_QUIT:
					should_quit = 1;
					break;
				case SDL_MOUSEMOTION:
					vscreen_dbg("X:%d Y:%d RX:%d RY:%d\n",
						    e.motion.x, e.motion.y,
						    e.motion.xrel,
						    e.motion.yrel);

					pthread_mutex_lock(&ctl->remote.mutex);
					if (SDL_GetRelativeMouseMode() ==
					    SDL_TRUE) {
						tp.x = e.motion.xrel;
						tp.y = e.motion.yrel;
						tp.is_abs = 0;
						trans(ctl, &tp);

						ve_add_new(ctl->remote.vc_mouse,
							   EV_REL, REL_X, tp.x);
						ve_add_new(ctl->remote.vc_mouse,
							   EV_REL, REL_Y, tp.y);
						do_sync_event(
							ctl->remote.vc_mouse);
					} else {
						tp.x = e.motion.x;
						tp.y = e.motion.y;
						tp.is_abs = 1;
						trans(ctl, &tp);

						ve_add_new(ctl->remote.vc_touch,
							   EV_ABS, ABS_X, tp.x);
						ve_add_new(ctl->remote.vc_touch,
							   EV_ABS, ABS_Y, tp.y);
						do_sync_event(
							ctl->remote.vc_touch);
					}
					pthread_mutex_unlock(
						&ctl->remote.mutex);

					break;
				case SDL_MOUSEBUTTONDOWN:
					switch (e.button.button) {
					case SDL_BUTTON_LEFT:
						vscreen_dbg(
							"Mouse left down at %d %d\n",
							e.button.x, e.button.y);

						/* Switch RelativeMoseMode */
						if (SDL_GetModState() &
						    KMOD_ALT)
							switch_mouse_mode();

						pthread_mutex_lock(
							&ctl->remote.mutex);
						if (SDL_GetRelativeMouseMode() ==
						    SDL_TRUE) {
							ve_add_new(
								ctl->remote
									.vc_mouse,
								EV_KEY,
								BTN_LEFT,
								TOUCH_DOWN);
							do_sync_event(
								ctl->remote
									.vc_mouse);
						} else {
							tp.x = e.button.x;
							tp.y = e.button.y;
							tp.is_abs = 1;
							trans(ctl, &tp);

							ve_add_new(
								ctl->remote
									.vc_touch,
								EV_KEY,
								BTN_TOUCH,
								TOUCH_DOWN);
							ve_add_new(
								ctl->remote
									.vc_touch,
								EV_ABS, ABS_X,
								tp.x);
							ve_add_new(
								ctl->remote
									.vc_touch,
								EV_ABS, ABS_Y,
								tp.y);
							do_sync_event(
								ctl->remote
									.vc_touch);
						}
						pthread_mutex_unlock(
							&ctl->remote.mutex);

						break;
					case SDL_BUTTON_RIGHT:
						vscreen_dbg(
							"Mouse right down at %d %d\n",
							e.button.x, e.button.y);

						/* Switch RelativeMoseMode */
						if (SDL_GetModState() &
						    KMOD_ALT)
							switch_mouse_mode();

						pthread_mutex_lock(
							&ctl->remote.mutex);
						if (SDL_GetRelativeMouseMode() ==
						    SDL_TRUE) {
							ve_add_new(
								ctl->remote
									.vc_mouse,
								EV_KEY,
								BTN_RIGHT,
								TOUCH_DOWN);
							do_sync_event(
								ctl->remote
									.vc_mouse);
						}
						pthread_mutex_unlock(
							&ctl->remote.mutex);
						break;
					case SDL_BUTTON_MIDDLE:
						vscreen_dbg(
							"Mouse middle down at %d %d\n",
							e.button.x, e.button.y);

						/* Switch RelativeMoseMode */
						if (SDL_GetModState() &
						    KMOD_ALT)
							switch_mouse_mode();

						pthread_mutex_lock(
							&ctl->remote.mutex);
						if (SDL_GetRelativeMouseMode() ==
						    SDL_TRUE) {
							ve_add_new(
								ctl->remote
									.vc_mouse,
								EV_KEY,
								BTN_MIDDLE,
								TOUCH_DOWN);
							do_sync_event(
								ctl->remote
									.vc_mouse);
						}
						pthread_mutex_unlock(
							&ctl->remote.mutex);
						break;
					default:
						vscreen_dbg(
							"Mouse other down at %d %d\n",
							e.button.x, e.button.y);
						break;
					}
					break;
				case SDL_MOUSEBUTTONUP:
					switch (e.button.button) {
					case SDL_BUTTON_LEFT:
						vscreen_dbg(
							"Mouse left up at %d %d\n",
							e.button.x, e.button.y);
						pthread_mutex_lock(
							&ctl->remote.mutex);
						if (SDL_GetRelativeMouseMode() ==
						    SDL_TRUE) {
							ve_add_new(
								ctl->remote
									.vc_mouse,
								EV_KEY,
								BTN_LEFT,
								TOUCH_UP);
							do_sync_event(
								ctl->remote
									.vc_mouse);
						} else {
							ve_add_new(
								ctl->remote
									.vc_touch,
								EV_KEY,
								BTN_TOUCH,
								TOUCH_UP);
							do_sync_event(
								ctl->remote
									.vc_touch);
						}
						pthread_mutex_unlock(
							&ctl->remote.mutex);
						break;
					case SDL_BUTTON_RIGHT:
						vscreen_dbg(
							"Mouse right up at %d %d\n",
							e.button.x, e.button.y);

						pthread_mutex_lock(
							&ctl->remote.mutex);
						if (SDL_GetRelativeMouseMode() ==
						    SDL_TRUE) {
							ve_add_new(
								ctl->remote
									.vc_mouse,
								EV_KEY,
								BTN_RIGHT,
								TOUCH_UP);
							do_sync_event(
								ctl->remote
									.vc_mouse);
						}
						pthread_mutex_unlock(
							&ctl->remote.mutex);
						break;
					case SDL_BUTTON_MIDDLE:
						vscreen_dbg(
							"Mouse middle up at %d %d\n",
							e.button.x, e.button.y);
						pthread_mutex_lock(
							&ctl->remote.mutex);
						if (SDL_GetRelativeMouseMode() ==
						    SDL_TRUE) {
							ve_add_new(
								ctl->remote
									.vc_mouse,
								EV_KEY,
								BTN_MIDDLE,
								TOUCH_UP);
							do_sync_event(
								ctl->remote
									.vc_mouse);
						}
						pthread_mutex_unlock(
							&ctl->remote.mutex);
						break;
					default:
						vscreen_dbg(
							"Mouse other up at %d %d\n",
							e.button.x, e.button.y);
						break;
					}
					break;
				case SDL_KEYDOWN:
					keycode = keymap[e.key.keysym.scancode];
					vscreen_dbg(
						"Physical %s (%d) key acting as %s key\n",
						SDL_GetScancodeName(
							e.key.keysym.scancode),
						e.key.keysym.scancode,
						SDL_GetKeyName(
							e.key.keysym.sym));
					if (keycode != 0) {
						pthread_mutex_lock(
							&ctl->remote.mutex);
						ve_add_new(
							ctl->remote.vc_keyboard,
							EV_KEY, keycode,
							TOUCH_DOWN);
						do_sync_event(
							ctl->remote.vc_keyboard);
						pthread_mutex_unlock(
							&ctl->remote.mutex);
					} else {
						vscreen_err(
							"Unknown keycode %s(%d)\n",
							SDL_GetScancodeName(
								e.key.keysym
									.scancode),
							e.key.keysym.scancode);
					}
					break;
				case SDL_KEYUP:
					keycode = keymap[e.key.keysym.scancode];
					if (keycode != 0) {
						pthread_mutex_lock(
							&ctl->remote.mutex);
						ve_add_new(
							ctl->remote.vc_keyboard,
							EV_KEY, keycode,
							TOUCH_UP);
						do_sync_event(
							ctl->remote.vc_keyboard);
						pthread_mutex_unlock(
							&ctl->remote.mutex);
					} else {
						vscreen_err(
							"Unknown keycode %s(%d)\n",
							SDL_GetScancodeName(
								e.key.keysym
									.scancode),
							e.key.keysym.scancode);
					}
					break;
				case SDL_MOUSEWHEEL:
					vscreen_dbg(
						"Mouse wheel x = %d y = %d\n",
						e.wheel.x, e.wheel.y);

					pthread_mutex_lock(&ctl->remote.mutex);
					ve_add_new(ctl->remote.vc_mouse, EV_REL,
						   REL_WHEEL, e.wheel.y);
					do_sync_event(ctl->remote.vc_mouse);
					pthread_mutex_unlock(
						&ctl->remote.mutex);
					break;
				}
			}

			sdl2_draw_frame(ctl);
		}

	} while (0);
}

void sdl2_destroy(SDL2_HANDLER handler)
{
	do {
		struct sdl2_ctl *ctl = handler2ctl(handler);

		if (ctl->texture)
			SDL_DestroyTexture(ctl->texture);

		if (ctl->render)
			SDL_DestroyRenderer(ctl->render);

		if (ctl->window)
			SDL_DestroyWindow(ctl->window);

		SDL_Quit();

		if (ctl)
			free(ctl);

	} while (0);
}
