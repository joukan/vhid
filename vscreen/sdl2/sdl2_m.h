#ifndef __SDL2_M_H__
#define __SDL2_M_H__

#include <SDL2/SDL.h>

typedef void *SDL2_HANDLER;
#define SDL2_INVALID_HANDLER (SDL2_HANDLER)0

enum rotate_drgee { ROTATE_0, ROTATE_90, ROTATE_180, ROTATE_270 };

struct sdl2_para {
	const char *dev;
	uint32_t src_w, src_h, dst_w, dst_h;
	uint32_t flag;
	enum rotate_drgee rotate;
	char *serverip;
	char *vhid_dev_name;
};

struct sdl2_frame {
	void *frame;
	int length;
};

#define FLG_USE_SOFTWARE_RENDER (1 << 0)
#define FLG_KEEP_RELXY (1 << 1)
SDL2_HANDLER
sdl2_create(struct sdl2_para *para);
void sdl2_event_loop(SDL2_HANDLER handler);
void sdl2_destroy(SDL2_HANDLER handler);
void sdl_update_frame(void *frame, int length);
#endif
