#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>
#include "v4l2_m.h"
#include "sdl2_m.h"

#include "v_dbg.h"

#define DEFAULT_DEVICE "/dev/video0"

void usage()
{
	fprintf(stderr,
		"vscreen [skh] [-c <vhid server ip>] [-d <video device>] [-r <rotation degree>\n");
	fprintf(stderr,
		"\t[-f <expected fps>] [-W <window width>] [-H <window height>\n");

	fprintf(stderr, "\n");
	fprintf(stderr,
		"  -W: Specify window width, also used as an V4l2 format hint. (default 1024)\n");
	fprintf(stderr,
		"  -H: Specify window height, also used as an V4l2 format hint. (default 768)\n");
	fprintf(stderr, "  -f: Expected FPS.\n");
	fprintf(stderr,
		"  -d: Specify video capture device. (default /dev/video0)\n");
	fprintf(stderr, "  -c: Specifiy vhid receiver ip address.\n");
	fprintf(stderr, "  -s: Use software render. (default no)\n");
	fprintf(stderr, "  -n: Specify vhid device name.\n");
	fprintf(stderr,
		"  -k: Do not translate mouse relative coordinates in rotation.\n");
	fprintf(stderr, "  -h: Show this help.\n");
}

static V4L2_HANDLER v4l2_handler;
static SDL2_HANDLER sdl2_handler;

void dump_yuyv422_frame(void *frame, int length)
{
	static int frame_no = 0;
	char frame_file[64];
	sprintf(frame_file, "frame_%d.yuv", frame_no++);

	int fd = open(frame_file, O_CREAT | O_RDWR, 0644);
	if (fd < 0)
		return;

	write(fd, frame, length);
	close(fd);
}

void draw_frame(void *frame, int length)
{
	assert(sdl2_handler != SDL2_INVALID_HANDLER);
#if 0
	vscreen_dbg("%s-%d frame: %p, length: %d\n", __func__, __LINE__, frame,
		    length);

	dump_yuyv422_frame(frame, length);
#endif
	sdl_update_frame(frame, length);
}

int main(int argc, char *const argv[])
{
	int c;
	int ret;
	uint32_t fps;
	struct sdl2_para para = { .dev = DEFAULT_DEVICE,
				  .src_w = 0,
				  .src_h = 0,
				  .dst_w = 1024,
				  .dst_h = 768,
				  .flag = 0,
				  .rotate = ROTATE_0,
				  .serverip = NULL,
				  .vhid_dev_name = NULL };

	vscreen_dbg_init();

	do {
		while ((c = getopt(argc, argv, "skhW:H:f:d:c:r:n:")) != -1) {
			switch (c) {
			case 'W':
				para.dst_w = (uint32_t)strtol(optarg, NULL, 10);
				break;
			case 'H':
				para.dst_h = (uint32_t)strtol(optarg, NULL, 10);
				break;
			case 'f':
				fps = (uint32_t)strtol(optarg, NULL, 10);
				break;
			case 'd':
				para.dev = optarg;
				break;
			case 'c':
				para.serverip = optarg;
				break;
			case 'r':
				if (!strcmp(optarg, "90"))
					para.rotate = ROTATE_90;
				else if (!strcmp(optarg, "180"))
					para.rotate = ROTATE_180;
				else if (!strcmp(optarg, "270"))
					para.rotate = ROTATE_270;
				else
					para.rotate = ROTATE_0;
				break;
			case 's':
				para.flag |= FLG_USE_SOFTWARE_RENDER;
				break;
			case 'k':
				para.flag |= FLG_KEEP_RELXY;
				break;
			case 'n':
				para.vhid_dev_name = optarg;
				break;
			case 'h':
				usage();
				exit(0);
				break;
			default:
				usage();
				exit(1);
				break;
			}
		}

		assert(para.dst_w);
		assert(para.dst_h);
		assert(para.dev);

		v4l2_handler =
			v4l2_create(para.dev, para.dst_w, para.dst_h, fps);
		if (v4l2_handler == V4L2_INVALID_HANDLER) {
			ret = 1;
			break;
		}

		para.src_w = v4l2_pixel_width(v4l2_handler);
		para.src_h = v4l2_pixel_heitht(v4l2_handler);

		sdl2_handler = sdl2_create(&para);

		assert(sdl2_handler != SDL2_INVALID_HANDLER);

		ret = v4l2_streamon(v4l2_handler, draw_frame);
		assert(ret == 0);

		sdl2_event_loop(sdl2_handler);
		v4l2_streamoff(v4l2_handler);
		v4l2_destroy(v4l2_handler);
		sdl2_destroy(sdl2_handler);

	} while (0);

	exit(ret);
}
