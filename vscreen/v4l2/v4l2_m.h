#ifndef __V4L2_M_H__
#define __V4L2_M_H__
#include <stdint.h>
#include <linux/videodev2.h>

typedef void *V4L2_HANDLER;
#define V4L2_INVALID_HANDLER (V4L2_HANDLER)0

typedef void (*draw_func_t)(void *frame, int length);

V4L2_HANDLER v4l2_create(const char *dev, uint32_t width, uint32_t height,
			 uint32_t fps);
int v4l2_streamon(V4L2_HANDLER handler, draw_func_t draw_func);
int v4l2_streamoff(V4L2_HANDLER handler);
void v4l2_destroy(V4L2_HANDLER handler);
uint32_t v4l2_pixel_width(V4L2_HANDLER handler);
uint32_t v4l2_pixel_heitht(V4L2_HANDLER handler);

#endif
