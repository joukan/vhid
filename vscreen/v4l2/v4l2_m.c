#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sys/ioctl.h>
#include <errno.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <assert.h>
#include <pthread.h>

#include "v_dbg.h"
#include "v4l2_m.h"

struct v4l2_buf {
	void *start;
	uint32_t length;
};

#define BUFNUM 4

struct v4l2_ctl {
	int fd;
	struct v4l2_buf buf[BUFNUM];
	uint32_t fps;
	struct v4l2_format fmt;
	struct v4l2_capability cap;
	struct v4l2_streamparm streamparm;
	pthread_t worker_thread;
	draw_func_t draw_func;
	char worker_thread_exit;
};

#define ctl2handler(ctl) (ctl ? (V4L2_HANDLER)ctl : V4L2_INVALID_HANDLER)
#define handler2ctl(handler) (struct v4l2_ctl *)handler

V4L2_HANDLER v4l2_create(const char *dev, uint32_t width, uint32_t height,
			 uint32_t fps)
{
	struct v4l2_ctl *ctl;
	int ret = -1;

	do {
		if (!dev)
			break;

		ctl = malloc(sizeof(*ctl));
		if (!ctl)
			break;

		memset(ctl, 0, sizeof(*ctl));

		ctl->fd = open(dev, O_RDWR | O_NONBLOCK, 0);
		if (ctl->fd < 0) {
			vscreen_err("Failed to open %s\n", dev);
			break;
		}

		/* 
		 * Check capability of V4L2_CAP_VIDEO_CAPTURE
		 */
		ret = ioctl(ctl->fd, VIDIOC_QUERYCAP, &ctl->cap);
		if (ret < 0) {
			vscreen_err("Failed to query capability of %s.\n", dev);
			break;
		}

		vscreen_info("Device %s capabilities:\n", dev);
		vscreen_info("%10s:\t%s\n", "Driver", ctl->cap.driver);
		vscreen_info("%10s:\t%s\n", "Card", ctl->cap.card);
		vscreen_info("%10s:\t%s\n", "Bus", ctl->cap.bus_info);
		vscreen_info("%10s:\t%u.%u.%u\n", "Version",
			     (ctl->cap.version >> 16) & 0xFF,
			     (ctl->cap.version >> 8) & 0xFF,
			     (ctl->cap.version >> 0) & 0xFF);
		vscreen_info("%10s:\t%lx\n", "Capability",
			     (unsigned long)ctl->cap.capabilities);
		vscreen_info("  %-20s:\t%s\n", "Streaming I/O",
			     ctl->cap.capabilities & V4L2_CAP_STREAMING ?
				     "yes" :
				     "no");
		vscreen_info("  %-20s:\t%s\n", "Video cap",
			     ctl->cap.capabilities & V4L2_CAP_VIDEO_CAPTURE ?
				     "yes" :
				     "no");
		vscreen_info("  %-20s:\t%s\n", "Video cap (MPLANE)",
			     ctl->cap.capabilities &
					     V4L2_CAP_VIDEO_CAPTURE_MPLANE ?
				     "yes" :
				     "no");
		vscreen_info("  %-20s:\t%s\n", "Video out",
			     ctl->cap.capabilities & V4L2_CAP_VIDEO_OUTPUT ?
				     "yes" :
				     "no");
		vscreen_info("  %-20s:\t%s\n", "Video out(MPLANE)",
			     ctl->cap.capabilities &
					     V4L2_CAP_VIDEO_OUTPUT_MPLANE ?
				     "yes" :
				     "no");
		vscreen_info("  %-20s:\t%s\n", "Video overlay",
			     ctl->cap.capabilities & V4L2_CAP_VIDEO_OVERLAY ?
				     "yes" :
				     "no");

		if ((ctl->cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) !=
		    V4L2_CAP_VIDEO_CAPTURE) {
			vscreen_err("%s does not support video capture\n", dev);
			break;
		}

		struct v4l2_fmtdesc desc;
		int found_yuvv = 0;
		desc.index = 0;
		desc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		vscreen_info("Capture format:\n");
		do {
			ret = ioctl(ctl->fd, VIDIOC_ENUM_FMT, &desc);
			vscreen_info("  %2d. %s\n", desc.index,
				     desc.description);
			desc.index++;
			if (desc.pixelformat == V4L2_PIX_FMT_YUYV)
				found_yuvv = 1;
		} while (ret != -1);

		if (!found_yuvv) {
			vscreen_err("%s does not support V4L2_PIX_FMT_YUYV\n",
				    dev);
			break;
		}

		ctl->fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ctl->fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
		ctl->fmt.fmt.pix.height = height;
		ctl->fmt.fmt.pix.width = width;
		ctl->fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

		ret = ioctl(ctl->fd, VIDIOC_S_FMT, &ctl->fmt);
		if (ret < 0) {
			vscreen_err("Failed to set v4l2 format.\n");
			break;
		}

		ret = ioctl(ctl->fd, VIDIOC_G_FMT, &ctl->fmt);
		if (ret < 0) {
			vscreen_err("Failed to set v4l2 format.\n");
			break;
		}

		if (height != ctl->fmt.fmt.pix.height)
			vscreen_warn("Pixel height changed to %lu.\n",
				     (unsigned long)ctl->fmt.fmt.pix.height);

		if (width != ctl->fmt.fmt.pix.width)
			vscreen_warn("Pixel width changed to %lu.\n",
				     (unsigned long)ctl->fmt.fmt.pix.width);

		vscreen_info("Pixel size: %lu %lu.\n",
			     (unsigned long)ctl->fmt.fmt.pix.width,
			     (unsigned long)ctl->fmt.fmt.pix.height);

		ctl->streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		/*
		 * This parameter is used to set the frame interval which is the
		 * time between two consecutive frames in a video stream.
		 *
		 * The frame interval is caculated as numerator / denominator.
		 * For example, if numerator = 1 and denominator = 30, the frame
		 * interval is 1/30 seconds, that is 30 fps.
		 */
		ctl->streamparm.parm.capture.timeperframe.numerator = 1;
		ctl->streamparm.parm.capture.timeperframe.denominator =
			fps != 0 ? fps : 30;
		ret = ioctl(ctl->fd, VIDIOC_S_PARM, &ctl->streamparm);
		if (ret < 0) {
			vscreen_err("Failed to set stream parameter.\n");
			break;
		}

		ret = ioctl(ctl->fd, VIDIOC_G_PARM, &ctl->streamparm);
		if (ret < 0) {
			vscreen_err("Failed to set stream parameter.\n");
			break;
		}

		ctl->fps =
			ctl->streamparm.parm.capture.timeperframe.denominator;
		if (fps && (ctl->fps != fps)) {
			vscreen_warn("FPS changed to %lu.\n",
				     (unsigned long)ctl->fps);
		} else {
			vscreen_info("FPS set to %lu.\n",
				     (unsigned long)ctl->fps);
		}

		struct v4l2_requestbuffers req;

		req.count = BUFNUM;
		req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		req.memory = V4L2_MEMORY_MMAP;
		ret = ioctl(ctl->fd, VIDIOC_REQBUFS, &req);
		if (ret < 0) {
			vscreen_err("Failed to request buffers.\n");
			break;
		}

		int i;
		struct v4l2_buffer buf;

		for (i = 0; i < BUFNUM; i++) {
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_MMAP;
			buf.index = i;

			ret = ioctl(ctl->fd, VIDIOC_QUERYBUF, &buf);
			if (ret < 0) {
				vscreen_err("Failed to query buffer.\n");
				break;
			}

			ctl->buf[i].length = buf.length;
			ctl->buf[i].start =
				mmap(NULL, buf.length, PROT_READ | PROT_WRITE,
				     MAP_SHARED, ctl->fd, buf.m.offset);

			if (ctl->buf[i].start == MAP_FAILED) {
				int j = i - 1;

				while (j >= 0) {
					munmap(ctl->buf[j].start,
					       ctl->buf[j].length);
					j--;
				}
				ret = -1;
				break;
			}
		}

		if (ret < 0)
			break;
	} while (0);

	if (ret < 0) {
		if (ctl->fd > 0)
			close(ctl->fd);

		free(ctl);
		ctl = NULL;
	}

	return ctl2handler(ctl);
}

static void *v4l2_worker(void *arg)
{
	do {
		struct v4l2_ctl *ctl = arg;
		struct v4l2_buffer buf;
		fd_set fds;
		int ret;

		assert(ctl);
		assert(ctl->draw_func);

		while (!ctl->worker_thread_exit) {
			FD_ZERO(&fds);
			FD_SET(ctl->fd, &fds);

			ret = select(ctl->fd + 1, &fds, NULL, NULL, NULL);
			if (ret < 0) {
				if (errno == EINTR)
					continue;

				vscreen_err("select error.\n");
				break;
			}

			if (FD_ISSET(ctl->fd, &fds)) {
				memset(&buf, 0, sizeof(buf));
				buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
				buf.memory = V4L2_MEMORY_MMAP;

				ret = ioctl(ctl->fd, VIDIOC_DQBUF, &buf);
				if (ret < 0) {
					vscreen_err("VIDIOC_DQBUF failed.\n");
					break;
				}

				ctl->draw_func(ctl->buf[buf.index].start,
					       ctl->buf[buf.index].length);

				buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
				buf.memory = V4L2_MEMORY_MMAP;

				ret = ioctl(ctl->fd, VIDIOC_QBUF, &buf);
				if (ret < 0) {
					vscreen_err("VIDIOC_QBUF failed.\n");
					break;
				}
			}
		}

	} while (0);

	return NULL;
}

int v4l2_streamon(V4L2_HANDLER handler, draw_func_t draw_func)
{
	int ret = -1;

	do {
		int i;
		struct v4l2_buffer buf;
		struct v4l2_ctl *ctl = handler2ctl(handler);

		if (!ctl)
			break;

		if (!draw_func)
			break;

		ctl->draw_func = draw_func;

		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;

		for (i = 0; i < BUFNUM; i++) {
			buf.index = i;
			ret = ioctl(ctl->fd, VIDIOC_QBUF, &buf);
			if (ret < 0) {
				vscreen_err("Failed to queue buffer.\n");
				break;
			}
		}

		if (ret < 0)
			break;

		enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(ctl->fd, VIDIOC_STREAMON, &type);
		if (ret < 0)
			vscreen_err("Failed to streamon.\n");

		ret = pthread_create(&ctl->worker_thread, NULL, v4l2_worker,
				     ctl);
		if (ret != 0) {
			vscreen_err("Failed to create worker thread.\n");
			break;
		}

	} while (0);

	return ret;
}

int v4l2_streamoff(V4L2_HANDLER handler)
{
	int ret = -1;

	do {
		int i;
		struct v4l2_ctl *ctl = handler2ctl(handler);

		if (!ctl)
			break;

		ctl->worker_thread_exit = 1;
		pthread_join(ctl->worker_thread, NULL);

		enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(ctl->fd, VIDIOC_STREAMOFF, &type);
		if (ret < 0)
			vscreen_err("Failed to streamoff.\n");

		ret = 0;
	} while (0);

	return ret;
}

void v4l2_destroy(V4L2_HANDLER handler)
{
	do {
		int i;
		struct v4l2_ctl *ctl = handler2ctl(handler);

		if (!ctl)
			break;

		for (i = 0; i < BUFNUM; i++)
			munmap(ctl->buf[i].start, ctl->buf[i].length);

		if (ctl->fd > 0)
			close(ctl->fd);

		free(ctl);
	} while (0);
}

uint32_t v4l2_pixel_width(V4L2_HANDLER handler)
{
	struct v4l2_ctl *ctl = handler2ctl(handler);

	assert(ctl);
	return ctl->fmt.fmt.pix.width;
}

uint32_t v4l2_pixel_heitht(V4L2_HANDLER handler)
{
	struct v4l2_ctl *ctl = handler2ctl(handler);

	assert(ctl);
	return ctl->fmt.fmt.pix.height;
}
